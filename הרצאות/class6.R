install.packages("ggplot2")
install.packages("deplyer")
install.packages("Amelia")
install.packages('deplyer', dependencies=TRUE, repos='http://cran.rstudio.com/')
install.packages('deplyer',repos='http://cran.us.r-project.org')

library(ggplot2)
library(Amelia)
library(deplyer)

titanic <- read.csv("train.csv")
str(titanic)

setRepositories()
setRepositories(ind = c(1:6, 8))
ap <- available.packages()

missmap(titanic,main = "missing data", col=c('yellow','black'))