#------Package List------
#------Chart------
install.packages("ggplot2")
library(ggplot2)
#------Linear Model------
install.packages('caTools')
library(caTools)
install.packages('dplyr')
library(dplyr)
#------Missing values------
install.packages('Amelia')
library(Amelia)
#------Correlations------ 
install.packages("corrplot")
library(corrplot)
#------Kmeans algorithm------
install.packages('cluster')
library(cluster)
#------SQL------
install.packages('RSQLite')
library(RSQLite)
install.packages('sqldf')
library(sqldf)
#------Text mining / Naive base------
install.packages('tm')
library(tm)
install.packages('e1071')
library(e1071)
install.packages('wordcloud')
library(wordcloud)
install.packages('SDMTools')
library(SDMTools)
#------Decision trees------
install.packages('rpart')
library(rpart)
install.packages('rpart.plot')
library(rpart.plot)
install.packages('ISLR')
library('ISLR')
#------Other Useful------
install.packages(c("caret","quanteda","irlba", "randomForetst"))
##############----------------------------------LINEAR MODEL------------------------------------------------------------
bike <- read.csv("bike.csv")
str(bike)
View(bike)
bike$datetime <- as.character(bike$datetime)
str(bike)
#Then we split date and time by blank 
bike$date <- sapply(strsplit(as.character(bike$datetime),' '), "[", 1)
bike$hour <- sapply(strsplit(as.character(bike$datetime),' '), "[", 2)
str(bike)
#We cast the date to date 
bike$date <- as.Date(bike$date)
#We take the number from the bike 
bike$hour <- sapply(strsplit(as.character(bike$hour),':'), "[", 1)
#We cast the hour to a number 
bike$hour <- as.numeric(bike$hour)

# Making season a factor 
# 1 = spring, 2 = summer, 3 = fall, 4 = winter 
bike$season <- factor(bike$season, levels = c(1,2,3,4), labels = c('spring', 'summer', 'fall','winter' ))
View(bike)
str(bike)
levels(bike$season)

#How temterature affects the count? 
ggplot(bike, aes(temp,count)) + geom_point(alpha = 0.8, aes(color = temp))#ככל שהטמפרטורה יותר גבוה הקאונט יותר  גבוה
#check the correlation between count and temp
cor(bike[,c('temp', 'count')])#the correlation is 0.4  which means no so high

#What regression line look like
plot(bike$temp,bike$count)
abline(lm(bike$count ~ bike$temp))

#How date affects the count? 
pl <- ggplot(bike, aes(date,count)) + geom_point(alpha = 0.5, aes(color = temp))
pl + scale_color_continuous(low = 'blue', high = 'red') +theme_bw()#we can see that in july the color is red witch mean high temp and high count rate

#How season affcets the count 
ggplot(bike, aes(season,count)) + geom_boxplot(aes(color = season)) + theme_bw()

#how does  the hour affect count on weekdays
pl <- ggplot(bike[bike$workingday != 1,], aes(hour,count))  #when the temp is high and the hour bewteewn 3 -8 the count is high
pl <- pl + geom_point(position = position_jitter(w= 0.5, h =0)  ,aes(color = temp))
pl <- pl + scale_colour_gradient(low = "blue", high = "red")
pl <- pl + theme_bw()

#On sunday
pl <- ggplot(bike[bike$workingday == 1,], aes(hour,count))  
pl <- pl + geom_point(position = position_jitter(w= 0.5, h =0)  ,aes(color = temp))
pl <- pl + scale_colour_gradient(low = "blue", high = "red")
pl <- pl + theme_bw()

####################################Split into training set snd test set 
dim(bike)# 10886 14
#10886*0.7=7620
bike$date[7620]# 2012-5-16

#we will divide 70% to train and 30% to test
splitDate <- as.Date("2012-05-16")
dateFilter <- bike$date <= splitDate 

bike.train <- bike[dateFilter,]
dim(bike.train)
bike.test <-  bike[!dateFilter,]
dim(bike.test)
dim(bike)

bike.train$datetime <-NULL
bike.test$datetime <-NULL


#lm run only in traning set!!!!!!
model <- lm(count ~ . -atemp -casual -registered -date, bike.train)

summary(model)
#חיזוי
predicted.train <- predict(model,bike.train)
predicted.test <- predict(model,bike.test)

#נבדוק את הסטייה
MSE.train <- mean((bike.train$count - predicted.train)**2)#15072
MSE.test <- mean((bike.test$count - predicted.test)**2)#40649
### significant overfitting ##

RMSE.train <- MSE.train**0.5
RMSE.test <- MSE.test**0.5

