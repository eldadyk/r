#------Package List------

#------Chart------
library(ggplot2)

#------Linear Model------
library(caTools)


library(dplyr)

#------Missing values------
install.packages('Amelia')
library(Amelia)

#------Correlations------ 
install.packages("corrplot")
library(corrplot)

#------Kmeans algorithm------
install.packages('cluster')
library(cluster)

#------SQL------
install.packages('RSQLite')
library(RSQLite)

install.packages('sqldf')
library(sqldf)

#------Text mining / Naive base------
install.packages('tm')
library(tm)

install.packages('e1071')
library(e1071)

install.packages('wordcloud')
library(wordcloud)

install.packages('SDMTools')
library(SDMTools)

#------Decision trees------
install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

install.packages('ISLR')
library('ISLR')

#------Other Useful------
install.packages(c("caret","quanteda","irlba", "randomForetst"))







#----------------------------------------1-Read the file use-------------------------------------
data <- read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv')) 
str(data)
summary(data)
#------------------------------------2-Reduce the number of levels for country-------------

levels(data$country)#מכניס לתוך ווקטור את כל המדינות שנמצאות בכל יבשת בנפרד
Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
            "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
            "Portugal")


Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
          "Taiwan", "Thailand" ,  "Laos", "India",  "Vietnam"    )


North.america <- c("United-States" ,  "Canada", "Puerto-Rico"  ) 

Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
                             "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )
Other <- c("South")

group_country <- function(ctry){
  if(ctry %in%  Europe) #אם הערך נמצא באירופה תחזיר אירופה. וכן הלאה.
  {return("Europe")} 
  else if (ctry %in% Asia)
  {return("Asia")}
  else if (ctry %in% North.america)
  {return("North.america")}
  else if (ctry %in% Latin.and.south.america)
  {return("Latin.and.south.america")}
  else 
  {return('Other')} 
}
data$country <- sapply(data$country,group_country )

table(data$country)

#-----3-Reduce the number of levels for ‘marital’  ( 3 levels ) and type_employer (5 levels)----------------------------
levels(data$marital)
levels(data$type_employer)
table(data$type_employer)

unemp <- function(job){#פונקציה שמורידה את הלבלס
  job <- as.character(job)
  if(job == 'Never-worked' | job == 'Without-pay'){
    return('Unemployed')
  }else{
    return(job)
  }
}
data$type_employer <- sapply(data$type_employer,unemp)#הזרקה של הפונקציה תחת הקטגוריה הרצוייה
table(data$type_employer)
str(data)


group_emp <- function(job){
  if(job == 'Local-gov' | job == 'State-gov' | job == 'Federal-gov' ){
    return('Gov')
  }else if(job == 'Self-emp-inc' | job == 'Self-emp-not-inc'){
    return('Self-emp')
  } else {
    return(job)
  }
}
data$type_employer <- sapply(data$type_employer,group_emp)
table(data$type_employer)
str(data)

levels(data$marital)
table(data$marital)

group_marital <- function(mar){
  mar <- as.character(mar)
  if(mar == 'Separated' | mar == 'Divorced' | mar == 'Widowed'){
    return('Not-married')
  }else if(mar == 'Never-married'){
    return(mar)
  } else {
    return('Married')
  }
}

data$marital <- sapply(data$marital,group_marital )
table(data$marital)
#--------------------------------4-Remove all records with missing data ----------------------------------------------
data[data=='?']<- NA#מזריק את הערך נאל במקום סימן שאלה כי זה הפירוש של סימן שאלה
any(is.na(data)) #true
library(Amelia)
missmap(data)#שימוש באמילייה לראות אם יש נאלים
data <- na.omit(data)
missmap(data)#ניתן להריץ שוב לראות שהריבוע יהיה חלק זאת אומרת נקי מנאלים

#------------5-Remove the index column from the dataset -------------------------------------------------------------------------------------
data <- select(data, -X)#הורדת עמודת האינדקס מהדאטאסט

#--------6-Use histograms to review the effect of the variables on the target attribute 
---------------------------------------------------------
  #רוצים לנתח את המשתנים עם משתנה המטרה ולכן צריך להחזיר את הערכים לפקטור  
str(data)
data$type_employer <- factor(data$type_employer)
data$country <- factor(data$country)
data$marital  <- factor(data$marital)
str(data)

pl <- ggplot(data,aes(age)) #מיפוי לפי הגיל
#נוסיף היסטוגרמה שתראה את התלות בהכנסה מעל 50 אלף או מתחת
pl <- pl+geom_histogram(aes(fill = income),color= 'black', binwidth = 1)

#-------------------------7-Split the dataset to train and test -----------
dim(data)
##דרך ראשונה-של רוני
sample <- sample.split(data$income, SplitRatio = 0.7)#משתנה המטרה שלנו הוא ההכנסה לכן בחרנו אותו מהדאטא
train <- subset(data, sample ==T)
test <- subset(data, sample ==F)
dim(train)
dim(test)
data.clean.train <-
##דרך שניה לקבוע טסט וטריין מהיוטיוב
sample2 <- sample(2, nrow(data), replace = T ,prob=c(0.7,0.3))
train <- data[sample2==1,]
test <- data[sample2==2,]


#----------------8-Compute the logistic regression model ---------
## if i want only 1 compare replace . with requested field
model.income <- glm(income ~ ., family = binomial(link = 'logit'), data = train)
summary(model.income)
step.model.income <- step(model.income)
summary(step.model.income)

#----10---Use the test set to find the error rate -----------------
predicted.train.income <- predict(model.income, train, type = 'response')
predicted.test.income <- predict(model.income, test, type = 'response')

predicted.Trainvalues <- ifelse(predicted.train.income > 0.5,1,0)
predicted.Testvalues <- ifelse(predicted.test.income > 0.5,1,0)

misClassError <- mean(predicted.Trainvalues != predicted.Testvalues)#יצא 32% זאת אומרת שטעיתי ב-32 אחוז בשאר צדקתי.
