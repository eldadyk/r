####################################EX-6################################################
install.packages('ggplot2')
library(ggplot2)
install.packages("ggplot2movies")
library(ggplot2movies)
install.packages('Amelia')
library(Amelia)
install.packages('dplyr')
library(dplyr)
#for correlations 
install.packages("corrplot")
library(corrplot)
install.packages("caTools")
library(caTools)
install.packages('ISLR')
library('ISLR')
install.packages('cluster')
library(cluster)
install.packages('RSQLite')
install.packages('sqldf')
library(RSQLite)
library(sqldf)
install.packages('tm')
library(tm)
install.packages('e1071')
library(e1071)
#---------------------------------------------1-Handling Hebrew-------------------------------
contact<-read.csv(file.choose(), stringsAsFactors = FALSE)
str(contact)
#---------------------------------------------2-#build the corpus pay attention to Hebrew and the dtm------------------------
text <- contact$text#isolate text column 
contact.corpus <- Corpus(VectorSource(contact$text), readerControl = list(language = "heb"))
dtm <- DocumentTermMatrix(contact.corpus)#build the dtm
inspect(dtm)
m <- as.matrix(dtm)
v <- sort(colSums(m), decreasing=TRUE)#look at the most frequent terms in the document
stopwords <- c(v[1],v[7],v[30])#generate stopwords from words that arefrequent and does not seem to carry any  meaning
# cleaning the corpus-------------------
clean_corpus <- tm_map(contact.corpus,removePunctuation)# remove punctuation
clean_corpus <- tm_map(clean_corpus,stripWhitespace)# remove whitespace characters 
clean_corpus <- tm_map(clean_corpus,removeNumbers)#remove digits
clean_corpus <- tm_map(clean_corpus,content_transformer(tolower))#turn to lower case (not very important in hebrew)
clean_corpus <- tm_map(clean_corpus,removeWords, stopwords)#remove stopwords 
dtm_test <- DocumentTermMatrix(clean_corpus)#insert to DTM
inspect(dtm_test)
dim(dtm_test)
frequent_dtm <-DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm_test,20)))#removing infrequent terms (that do not appear at least 20 times)
dim(dtm_test)
#בגלל שהפונקציה לא עובדת על טקטס בעברית נשתמש בדרך חלופית
dtm_mat <- as.matrix(dtm_test)
sums <- colSums(dtm_mat)
column_filter <- sums > 20
dtm_mat_final <- dtm_mat[,column_filter]
#----------------------------------------------
#Let's see what came out:
dim(dtm_mat_final)
View(dtm_mat_final)
dtm_mat_final[1:5,1:5]

#now we can work on category
contact$category <- as.factor(contact$category )
levels(contact$category)#there are records with empty category , lets remove them
filter_not_empty <- contact$category != ""

contact_new <- contact[filter_not_empty, ]#new df without the empty rows 
levels(contact_new$category)

contact_new <- droplevels(contact_new)#הפונקציה הקודמת לא הסירה את הרווח ולכן נשתמש בפונקציה נוספת
levels(contact_new$category)

add_column <- function(df,category){#A function to add a column
  new_column <- ifelse(df$category == category,1,0)
  new_column <- as.factor(new_column)
  df[category] <- new_column
  return(df)
}
#Actually adding the columns 
contact_new <- add_column(contact_new,'sales')
contact_new <- add_column(contact_new,'block')
contact_new <- add_column(contact_new,'coop')
contact_new <- add_column(contact_new,'stop')
contact_new <- add_column(contact_new,'support')
str(contact_new)
levels(contact_new$category)

#-----------------------------------------spliting traning set and test set--------------
vec <- runif(nrow(dtm_mat_final))
split <- vec > 0.3
##---------------------------------------prepre the data to naiveBayes-----------
#splitting the raw data
train_raw <- contact_new[split,]#70%
dim(train_raw)
View(train_raw)
test_raw <- contact_new[!split,]#30%
dim(test_raw)

#splitting dtm
train_dtm <- dtm_mat_final[split,]
test_dtm <- dtm_mat_final[!split,]
nrow(train_dtm)
nrow(test_dtm)

#convert the frequency matrix to yes/no  
conv_yesno <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x, level = c(1,0), labels = c('Yes', 'No'))
}

train <- apply(train_dtm, MARGIN = 1:2, conv_yesno)#תעבור על כל ערך במטריצה לחוד
test <- apply(test_dtm, MARGIN = 1:2, conv_yesno)
View(dtm_mat_final)
dim(train)
dim(test)
#---------------------------convert to data frame for R----------
df_train = as.data.frame(train)
df_test = as.data.frame(test)
View(df_train[,1:226])

df_train <- cbind(df_train,train_raw[,2:7])#מאחד את הטבלאות בלי השורה הרשונה שהיא הטקסט עצמו  
dim(df_train)
View(df_train)
df_test <- cbind(df_test,test_raw[,2:7])
dim(df_test)
#####------------------------start naiv bayes----------------------------------------------
model.sales <- naiveBayes(df_train,df_train$sales)#תפעיל את נאיב בייס על כל השורות ועל 383 העמודות
model.block <- naiveBayes(df_train[,1:383],df_train$block)
model.coop <- naiveBayes(df_train[,1:383],df_train$coop)
model.stop <- naiveBayes(df_train[,1:383],df_train$stop)
model.support <- naiveBayes(df_train[,1:383],df_train$support)

#test each model separately 
pred.sales <- predict(model.sales, df_test[,1:383])
pred.sales.raw <-predict(model.sales, df_test[,1:383], "raw")
table(pred.sales,df_test$sales)

pred.block <- predict(model.block, df_test[,1:383])
pred.block.raw <- predict(model.block, df_test[,1:383], "raw")
table(pred.block,df_test$block)

pred.coop <- predict(model.coop, df_test[,1:383])
pred.coop.raw <- predict(model.block, df_test[,1:383], "raw")
table(pred.coop,df_test$coop)


pred.stop <- predict(model.stop, df_test[,1:383])
pred.stop.raw <- predict(model.stop, df_test[,1:383], "raw")
table(pred.stop,df_test$stop)
#רשימה של כל המודלים
models <- list(model.sales,model.block, model.coop, model.stop, model.support)
df <- df_test[,1:383];

#The functions that aggregates all the models
predict.all <- function(models,df){
  pred.sales.raw <-predict(models[[1]], df, "raw")[,2] 
  pred.block.raw <-predict(models[[2]], df, "raw")[,2]
  pred.coop.raw <-predict(models[[3]], df, "raw")[,2]
  pred.stop.raw <-predict(models[[4]], df, "raw")[,2]
  pred.support.raw <-predict(models[[5]], df, "raw")[,2]
  mat <- rbind(pred.sales.raw,pred.block.raw,pred.coop.raw,pred.stop.raw,pred.support.raw)
  names <- c('sales','block', 'coop', 'stop', 'support')
  rownames(mat) <- names
  #find the row name with the highest value
  return (rownames(mat)[apply(mat,2,which.max)])
}
predicted <- predict.all(models,df_test[,1:383])

#look for errors 
table(predicted,test_raw$category)


