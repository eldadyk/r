# Use dataset airquality (part of the R bundle) 

# 1.Calculate the number of rows with NA�s
# 2.Remove all the rows with NA�s
# 3.Produce a vector with all average measures (Ozone Solar.R Wind Temp)
# 4.Filter the dataset for rows that are above average in temperature 
# 5.Sort the dataset according to solar radiation 
# 6.Add a column to the dataset that shows a ratio between Ozone and Solar.R (use the apply function properly) 
# 7.Plot on the same page all 6 graphs of one attribute against another (e.g. Ozone against Solar.R). From the graphs which variables seem related? 
  
  





summary(airquality)
str(airquality)
data("airquality")
#1 calculate number on rows with NA's 
sum(is.na(airquality))

#2.Remove all the rows with NA's
airquality.clean <- na.omit(airquality)
nrow(airquality.clean)
data("airquality")
#3. a vector with all average measures
mean.vector <- c(mean(airquality.clean$Ozone),mean(airquality.clean$Solar.R),mean(airquality.clean$Wind),mean(airquality.clean$Temp))

#4 Filter the dataset for rows that are above average in temperature
first<-subset(airquality.clean,Ozone>42.09910)
second<-subset(first,Solar.R>184.80180)  
third<-subset(second,Wind>9.93964) 
finall<-subset(third,Temp>77.79279) 

#5. Sort the dataset according to solar radiation 
before.filter<-finall[order(finall$Solar.R),]
after.filter<-before.filter[1:4]

#6 .Add a column to the dataset that shows a ratio between Ozone and Solar.R 
ratio<-after.filter$Ozone/after.filter$Solar.R
cbind(after.filter,ratio)   

#7 Plot on the same page all 6 graphs of one attribute against another (e.g. Ozone against Solar.R). From the graphs which variables seem related?
#answer: Temp & Ozone
layout(graphics.off())
par(mfcol = c(3,2))
plot(after.filter$Ozone ,after.filter$Solar.R, pch=16, col=2, xlab="Ozone",ylab="Solar.R" )
Solar.Ozone <- lm(after.filter$Solar.R~after.filter$Ozone)
abline(coef(Solar.Ozone),lwd=2, col="black") 
ranks.Ozone <- order(after.filter$Ozone)
lines(after.filter$Ozone[ranks.Ozone],after.filter$Solar.R[ranks.Ozone], col="green")

plot(after.filter$Ozone ,after.filter$Wind, pch=16, col=2, xlab="Ozone",ylab="Wind" )
Wind.Ozone <- lm(after.filter$Wind~after.filter$Ozone)
abline(coef(Wind.Ozone),lwd=2, col="black") 
lines(after.filter$Ozone[ranks.Ozone],after.filter$Wind[ranks.Ozone], col="green")

plot(after.filter$Ozone ,after.filter$Temp, pch=16, col=2, xlab="Ozone",ylab="Temp" )
Temp.Ozone <- lm(after.filter$Temp~after.filter$Ozone)
abline(coef(Temp.Ozone),lwd=2, col="black") 
lines(after.filter$Ozone[ranks.Ozone],after.filter$Temp[ranks.Ozone], col="green")

plot(after.filter$Solar.R ,after.filter$Wind, pch=16, col=2, xlab="Solar.R",ylab="Wind" )
Wind.Solar <- lm(after.filter$Wind~after.filter$Solar.R)
abline(coef(Wind.Solar),lwd=2, col="black") 
ranks.Solar <- order(after.filter$Solar.R)
lines(after.filter$Solar.R[ranks.Ozone],after.filter$Wind[ranks.Ozone], col="green")

plot(after.filter$Solar.R ,after.filter$Temp, pch=16, col=2, xlab="Solar.R",ylab="Temp" )
Temp.Solar <- lm(after.filter$Temp~after.filter$Solar.R)
abline(coef(Temp.Solar),lwd=2, col="black") 
lines(after.filter$Solar.R[ranks.Ozone],after.filter$Temp[ranks.Ozone], col="green")

plot(after.filter$Temp ,after.filter$Wind, pch=16, col=2, xlab="Temp",ylab="Wind" )
Wind.Temp <- lm(after.filter$Wind~after.filter$Temp)
abline(coef(Wind.Temp),lwd=2, col="black") 
ranks.Temp <- order(after.filter$Temp)
lines(after.filter$Temp[ranks.Ozone],after.filter$Wind[ranks.Ozone], col="green")