
install.packages('tm')
install.packages('wordcloud')
install.packages('e1071')
install.packages('randomForest')
library(tm)
library(wordcloud)
library(e1071)

install.packages('rpart')
library(rpart)
library(rpart.plot)




spam<-read.csv('spam.csv', stringsAsFactors = FALSE)
spam$type <- as.factor(spam$type)
#analayse the spam data frame
str(spam)
head(spam)
# we transform the text part into a curpus
# vector source states that the input is a vector
# the corpus optimizes the data structure for text operations
# such as some text trnasformations ans extructing a document term matrix
spam_corpus <- Corpus(VectorSource(spam$text))
#looking at the corpus whuch is basically a list 
spam_corpus[[1]][[1]]
spam_corpus[[1]][[2]]
# cleainin the corpus
# remove puctuation
clean_corpus <- tm_map(spam_corpus,removePunctuation)
#remove digits
clean_corpus <- tm_map(clean_corpus,removeNumbers)
#turn to lower case
clean_corpus <- tm_map(clean_corpus,content_transformer(tolower))
#remove stopwords 
clean_corpus <- tm_map(clean_corpus,removeWords, stopwords())
#  Multiple whitespace characters are collapsed to a single blank
clean_corpus <- tm_map(clean_corpus,stripWhitespace)
stopwords()
#generate the document matrix 
dtm <- DocumentTermMatrix(clean_corpus)
#inspet the dtm 
dim(dtm)
#removing infrequent terms (that do not apear at least 10 times)
frequent_dtm <-DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,10)))

#inspet the frequent_dtm
dim(frequent_dtm)
inspect(frequent_dtm[1:500,1:20])

#Spliting to training and testing data sets
vec <- runif(500)
split <- vec > 0.3
split
#spliting the raw data
train_raw <- spam[split,]
dim(train_raw)
test_raw <- spam[!split,]
dim(test_raw)
#spliting the corpus data (Not sure we nned that)
train_corpus <- clean_corpus[split]
train_corpus
test_corpus <- clean_corpus[!split]
test_corpus
#spliting document term matrix 
train_dtm <- frequent_dtm[split,]
test_dtm <- frequent_dtm[!split,]
inspect(train_dtm[1:25,1:10])
inspect(test_dtm[1:25,1:10])
#convert the frequency matrix to yes/no  
conv_yesno <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x, level = c(1,0), labels = c('Yes', 'No'))
}

train <- apply(train_dtm, MARGIN = 1:2, conv_yesno)
test <- apply(test_dtm, MARGIN = 1:2, conv_yesno)
#note: not anymore document term matrix 
dim(test)
dim(train)
#convert the matrix into data frames 
df_train = as.data.frame(train)
df_test = as.data.frame(test)
#add type (ham or spam) column 
df_train$type <- train_raw$type
df_test$type <- test_raw$type
df_train[1:10,59:60]
#apllying naiive base
#note: column 60 is the class

###########Naive Bayes

model.nb <- naiveBayes(df_train[,-60], df_train$type)
model.nb
prediction.nb <- predict(model.nb,df_test[,-60])

#see the probability
predictedprob.nb <- predict(model.nb, df_test[,-60], type ='raw')
#see only spam records
predictedprob.nb[,'spam']

install.packages('rpart')
library(rpart)
#building roc chart
rocCurve.nb <- roc(test_raw$type, predictedprob.nb[,'spam'], levels = c("spam","ham"))
plot(rocCurve.nb, col="yellow", main='ROC chart')
#--------------------------------------------

#########Decision tree
str(df_train)
#generate the model
tree <- rpart(type~ ., method = 'class', data = df_train)

prp(tree)

dim(df_test)
prediction_tree <- predict(tree,df_test[,-60])
prediction_tree <- as.data.frame(prediction_tree)
prediction_tree[,'spam']

rocCurve.tree <- roc(test_raw$type, prediction_tree[,'spam'], levels = c("spam","ham"))

plot(rocCurve.tree, col = "red", main = "Roc Chart")
#-------------------------------------

################Random Forest:

rf.model<-randomForest(type~., data=df_train)
print(rf.model)

predicted<-predict(rf.model,newdata =  df_test)

predictedProd<-predict(rf.model, df_train, type='prob')

predictedProd_1<- predictedProd[,'spam']

rocCurve<-roc(train_raw$type, predictedProd_1,levels = c("ham","spam"))

#plot the chart
plot(rocCurve,col="purple", main = "ROC chart")


########Plot all in the same chart######
plot(rocCurve, col="yellow", main='ROC chart')
plot(rocCurve.nb, col = 1, lty = 2,add = TRUE)
plot(rocCurve.tree, col = 4, lty = 3, add = TRUE)
plot(rocCurve.tree, col = 4, lty = 3, add = TRUE)