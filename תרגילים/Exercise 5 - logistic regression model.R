
#The task in this exercise is to build a model to predict the income of citizens according to a census. The prediction should be whether the income is above or below 50K dollars. 

#These are the fields: 

# age  The age of the individual
# type_employer  The type of employer the individual has. Whether they are government, military, private, an d so on.
# fnlwgt  The \# of people the census takers believe that observation represents. We will be ignoring this variable
# education  The highest level of education achieved for that individual
# education_num  Highest level of education in numerical form
# marital  Marital status of the individual
# occupation  The occupation of the individual
# relationship  A bit more difficult to explain. Contains family relationship values like husband, father, and so on, but only contains one per observation. Im not sure what this is supposed to represent
# race  descriptions of the individuals race. Black, White, Eskimo, and so on
# sex  Biological Sex
# capital_gain  Capital gains recorded
# capital_loss  Capital Losses recorded
# hr_per_week  Hours worked per week
# country  Country of origin for person
# income  Boolean Variable. Whether or not the person makes more than \$50,000 per annum income.


#----------------------------------------------------------------------------------------------------




adult <- read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv'))
str(adult)
#Reduce the number of levels for country 

Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
            "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
            "Portugal")


Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
          "Taiwan", "Thailand" ,  "Laos", "India",  "Vietnam"    )


North.america <- c("United-States" ,  "Canada", "Puerto-Rico"  ) 

Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
                             "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )
Other <- c("South")

#And also use the operator %in% in the following manner: if(ctry %in%  Europe)

#Q1 - Reduce the number of levels for ΒmaritalΒ (3 levels) and type_employer (5 levels)
levels(adult$marital)
levels(adult$type_employer)
levels(adult$marital) <- c("Not-married", "Married", "Married", "Married", "Never-married", "Not-married", "Not-married")
levels(adult$type_employer) <- c("Private", "Gov", "Gov", "Never-worked", "Private", "Self", "Self", "Gov", "Without-pay" )

#Q2 - Remove all records with missing data 
install.packages('Amelia')
library(Amelia)
install.packages('dplyr')
library(dplyr)
missmap(adult, main = "Missind Data", col = c('yellow','black'))
any(is.na(adult)) #False

#Q3 - Remove the index column from the data set
adult.clean <- select(adult, -X)

#Q4 - Use histograms to test the effect of the variables on the target property
#target property = income
adult.clean <- select(adult.clean, -fnlwgt)
str(adult.clean)
library(ggplot2)
ggplot(adult.clean, aes(x=adult.clean$age, fill=adult.clean$income)) + geom_histogram(alpha=0.2, position="identity", binwidth = 0.5)
ggplot(adult.clean, aes(x=adult.clean$education_num, fill=adult.clean$income)) + geom_histogram(alpha=0.2, position="identity", binwidth = 0.5)

#Q5 - Split the data set to practice and test
adult.clean.train <- sample_frac(adult.clean, 0.7)
sid <- as.numeric(rownames(adult.clean.train))
adult.clean.test <- adult.clean[-sid,]

#Q6 - Calculation of logistic regression model
log.model <- glm(income ~ . , family = binomial(link = 'logit'), adult.clean.train)
summary(log.model)

predict.probabilities <- predict(log.model, adult.clean.test, type = 'response')
predict.values <- ifelse(predict.probabilities > 0.5 ,1,0)

#Q7 - Use the test set to find the error rate
misClassError <- mean(predict.values != adult.clean.test$income)


#misClassError = 1











#Q7 - Split the dataset to train and test

adult.clean.train <- sample_frac(adult.clean, 0.7)
sid <- as.numeric(rownames(adult.clean.train))
adult.clean.test <- adult.clean[-sid,]

#Q8 - Compute the logistic regression model

log.model <- glm(income ~ . , family = binomial(link = 'logit'), adult.clean.train)
summary(log.model)

predict.probabilities <- predict(log.model, adult.clean.test, type = 'response')
predict.values <- ifelse(predict.probabilities > 0.5,">50K","<=50K")

#Q9 - Use the test set to find the error rate

misClassError <- mean(predict.values != adult.clean.test$income)
#misClassError = 0.15




