spam <- read.csv("spam.csv", header=TRUE, sep=",", stringsAsFactors = FALSE)
View(spam)
str(spam)
install.packages('tm')
install.packages('wordcloud')
install.packages('e1071')
install.packages('randomForest')
library(tm)
library(wordcloud)
library(e1071)

#Naive Bayes Algorithm
#Spam filter example


#we convert the var target into a factor because its an numeric number we want to forcast what is the text category
spam$type<-as.factor(spam$type)
View(spam$type)

#Generating DTM

#first we need to change our data set to Corpus
spam_corpus <-Corpus(VectorSource(spam$text))
spam_corpus[[1]][[1]]
spam_corpus[[1]][[2]]

spam_corpus[[2]][[1]]
#clean out all ','
clean_corpus <-tm_map(spam_corpus, removePunctuation)
#check it out
clean_corpus[[1]][[1]]


#get rid of CAPS
clean_corpus <-tm_map(clean_corpus, content_transformer(tolower))
clean_corpus[[1]][[1]]

#get rid stopWords
clean_corpus <-tm_map(clean_corpus, removeWords, stopwords())
clean_corpus[[1]][[1]]

#clean -  strip wide spaces
clean_corpus <-tm_map(clean_corpus, stripWhitespace)
clean_corpus[[1]][[1]]

#create DTM
dtm<-DocumentTermMatrix(clean_corpus)
dim(dtm)

#remove infrequent word
frequent_dtm<-DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,10)))
dim(frequent_dtm)

#spliting into training&test set
# the train set need to be 70% from the data and the test set need to be 30% from the data (randomize)
split<-runif(500)
#70% will be true
#train set
split<-split >0.3

#dividing raw data
train_raw <- spam[split,]
test_raw <- spam[!split,]

#dividing the DTM
train_dtm <- frequent_dtm[split,]
test_dtm <- frequent_dtm[!split,]


# convert the DTM into yes/no
conv_yesno<-function(x){
  x<-ifelse(x>0,1,0)
  x<-factor(x,levels = c(1,0), labels = c('Yes', 'No'))
}

train <- apply(train_dtm,MARGIN = 1:2,conv_yesno)
test <- apply(test_dtm,MARGIN = 1:2,conv_yesno)

#check it
View(train)
View(test)


#coverting to Data Frame
df_train <- as.data.frame(train)
df_test<-as.data.frame(test)

#add the type colmn

View(train_raw)
View(test_raw)
View(train_raw$type)
View(df_train)

df_train$type <-train_raw$type
df_test$type <-test_raw$type
dim(df_train)
df_train[,58]

#Decision Tree model 
#-----------------------------------------------------------------------------------
install.packages('rpart')
library(rpart)
library(rpart.plot)

model.tree <-rpart(type ~ .,df_train)
prp(model.tree)

predict.tree <- predict(model.tree,df_test)
predictprob.tree <- predict(model.tree,df_test,type ='prob')

#see only spam records
predictprob.tree[,'spam']

#roc curv
install.packages('pROC')
library(pROC)

#building roc chart
rocCurve.tree <- roc(df_test$type, predictprob.tree[,'spam'], levels = c("ham","spam"))
plot(rocCurve.tree, col="yellow", main='ROC chart')

#Random Forest model
#---------------------------------------------------------------------------------
  install.packages('randomForest')
  library(randomForest)

#runing the random forest model
# you use with the train data set to run the model
View(df_train)
levels(df_test$type) <- levels(df_train$type)
model.rf <- randomForest(type ~ . ,data= df_train)
#gives MSE and confusion matrix
print(model.rf)  

# you use with the test data set to run the prediction by the model that you want
View(df_test)
View(df_test$type)
View(df_train$type)


predicted.rf <- predict(model.rf, df_test)
#see the probability
predictedprob.rf <- predict(model.rf,df_test, type ='prob')
#see only spam records
predictedprob.rf[,'spam']

#roc curve
#install.packages('pROC')
library(pROC)

#building roc chart
rocCurve.rf <- roc(df_test$type, predictedprob.rf[,'spam'], levels = c("ham","spam"))
plot(rocCurve.rf, col="blue", main='ROC chart', add = T)


#---------------------random forest2---------------------

rf.model<-randomForest(type~., data=df_train)
print(rf.model)

predicted<-predict(rf.model,newdata =  df_test)

predictedProd<-predict(rf.model, df_train, type='prob')

predictedProd_1<- predictedProd[,'spam']

rocCurve<-roc(train_raw$type, predictedProd_1,levels = c("ham","spam"))

#plot the chart
plot(rocCurve,col="purple", main = "ROC chart")






#Naive Bayes
#------------------------------------------------------------
model.nb <-naiveBayes(df_train[,-58],df_train$type)
prediction.nb <- predict(model.nb,df_test[,-58])

#see the probability
predictionprob.nb <- predict(model.nb,df_test[,-58], type ='raw')
#see only spam records
predictionprob.nb[,'spam']

#roc curv
install.packages('pROC')
library(pROC)

#building roc chart
View(predictionprob.nb[,'spam'])
View(df_test$type)
rocCurve.nb <- roc(df_test$type, predictionprob.nb[,'spam'], levels = c("ham","spam"))
plot(rocCurve.nb, col="red", main='ROC chart', add = T)

