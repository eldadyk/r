#Q1
mtcars
str(mtcars)
median <-median(mtcars$mpg)

#convert the frequency matrix to yes/no if the value is bigger then median  
conv_yesno <- function(x){
  x <- ifelse(x>median,1,0)
  x <- factor(x, level = c(1,0), labels = c('Yes', 'No'))
}
conv_yesno2 <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x, level = c(1,0), labels = c('Yes', 'No'))
}
mtcars$mpg <- sapply(mtcars$mpg, conv_yesno)
str(mtcars)
summary(mtcars)

#Q2-אלגוריתם עץ החלטות
install.packages('rpart.plot')
install.packages('rpart')
library(rpart.plot)
library(rpart)
tree <- rpart(mpg ~ .,method = 'class' ,mtcars)
prp(tree)#plot the tree model 
#מסקנה: ניתן לראות ש דאבליו טי הוא הגורם המשמפיע ביותר  עבור ערכים שלו שקטנים מ 3.3

#Q3-k means with cluster and confusion matrix
df.cluster <- mtcars[,-1]
str(df.cluster)
articleClaster <- kmeans(df.cluster,2, nstart = 20)
table(articleClaster$cluster, mtcars$mpg)
install.packages('ggplot2')
install.packages('cluster')
library(cluster)

clusplot(mtcars,articleClaster$cluster, color =T, shade = T, labels = 0, lines=0)
#ניסינו לחלק למספר גדול יותר מ-2  אשכולות אך מספר הטעויות היה זהה ולכן 2 אשכולות הינה תוצאה מספיקה

#Q4
max(mtcars$gear)
min(mtcars$gear)

mtcars$cyl <- cut(mtcars$cyl,breaks = c(0,4,6,8), labels = c('min','midd','max'))
mtcars$disp <- cut(mtcars$disp,breaks = c(71.1,120,250,472), labels = c('slow','avg','fast'))
mtcars$hp <- cut(mtcars$hp,breaks = c(0,52,150,335), labels = c('weak','standart','Power Turbo'))
mtcars$drat <- cut(mtcars$drat,breaks = c(0,2.76,3.50,4.93), labels = c('low ratio','avg ratio','high ratio'))
mtcars$wt <- cut(mtcars$wt,breaks = c(0,1.513,3.50,5.424), labels = c('light','heavey','super heavey'))
mtcars$qsec <- cut(mtcars$qsec,breaks = c(0,14.5,18,22.9), labels = c('saver','avg consum','drinker'))
mtcars$vs <- sapply(mtcars$vs, conv_yesno2)
mtcars$am <- sapply(mtcars$am, conv_yesno2)
mtcars$gear <- cut(mtcars$gear,breaks = c(0,3,4,5), labels = c('3 Gear','4 Gear','5 Gear'))
mtcars$carb <- cut(mtcars$carb,breaks = c(0,1,2,4), labels = c('Low','Mid','high'))

View(mtcars)
